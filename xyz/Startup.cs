﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(xyz.Startup))]
namespace xyz
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
